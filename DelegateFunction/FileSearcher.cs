﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;

namespace DelegateFunction
{
    public class FileArgs : CancelEventArgs
    {
        public string FileName { get; set; }
        public string Message { get; set; }
    }
    public class FileSearcher
    {
        public event EventHandler<FileArgs> FileFound;
        public event EventHandler<FileArgs> EndFind;

        private readonly string _rootPath;
        public FileSearcher(string rootPath)
        {
            _rootPath = rootPath;
        }

        public void StartSearch()
        {
            var files = Directory.GetFiles(_rootPath, "*", SearchOption.AllDirectories);

            FileArgs searcherEventArgs = new FileArgs();
            foreach (var file in files)
            {
                searcherEventArgs.FileName = file;
                FileFound?.Invoke(this, searcherEventArgs);
                if (searcherEventArgs.Cancel) { break; }
            }
            searcherEventArgs.Message = searcherEventArgs.Cancel ? "Поиск прерван!" : "Поиск завершен!";
            EndFind?.Invoke(this, searcherEventArgs);
        }
    }
}
