﻿using System;
using System.Collections.Generic;

namespace DelegateFunction
{
    class Program
    {
        static void Main(string[] args)
        {
            var fs = new FileSearcher(@"C:\GIT");

            fs.FileFound += Fs_FileFound;
            fs.EndFind += Fs_EndFind;

            fs.StartSearch();

            Console.Read();
        }
        private static void Fs_FileFound(object sender, FileArgs e)
        {
            Console.WriteLine(e.FileName);
            if (e.FileName.Length <= 70) return;
            e.Cancel = true;
        }
        private static void Fs_EndFind(object sender, FileArgs e)
        {
            Console.ForegroundColor = e.Cancel ? ConsoleColor.Red : ConsoleColor.White;
            Console.WriteLine(e.Message);
        }
    }
}
