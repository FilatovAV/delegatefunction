﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelegateFunction
{
    public static class Extension
    {
        
        //public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        //{
        //    var a = new object();


        //    var res = e.FirstOrDefault(f => f == getParametr);

        //    foreach (var item in e)
        //    {
        //        var a = float.Parse(item);
        //    }

        //    return (T)a;
        //}

        private static float GetMax(float a, float b)
        {
            return a > b ? a : b;
        }
    }
}
